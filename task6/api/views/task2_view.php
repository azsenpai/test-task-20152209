<?php ob_start(); ?>

<?php if (count($result) == 0): ?>
	Ничего не найдено
<?php else: ?>
<table class="table table-bordered table-hover table-striped">
	<thead>
		<tr>
			<th>F_ID</th>
			<th>ФИО</th>
			<th>Количество детей</th>
		</tr>
	</thead>
	<tbody>
		<?php for ($i = 0, $n = count($result); $i < $n; $i ++): ?>
		<tr>
			<td><?php print $result[$i]['F_ID']; ?></td>
			<td><?php print $result[$i]['FullName']; ?></td>
			<td><?php print $result[$i]['count']; ?></td>
		</tr>
		<?php endfor; ?>
	</tbody>
</table>
<?php endif; ?>

<?php

$output = ob_get_contents();
ob_end_clean();

return $output;

?>