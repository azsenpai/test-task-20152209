<?php

function  get_parents($keyword)
{
	$dbh = open_db();

	$sql = '
		SELECT F_ID AS id, FullName AS name
		FROM Faces
		WHERE FullName LIKE :keyword
	';

	$stmt = $dbh->prepare($sql);
	$stmt->bindParam(':keyword', $keyword);

	$result = array();

	if ($stmt->execute()) {
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	close_db($dbh);

	return $result;
}

function task2()
{
	$dbh = open_db();

	$sql = '
		SELECT T1.F_ID, T1.FullName, IFNULL(T2.count, 0) AS count
		FROM Faces AS T1 LEFT JOIN (
			SELECT F_ID, COUNT(*) AS count
			FROM (
				SELECT F_ID
				FROM Kids
				WHERE YEAR(CURRENT_TIMESTAMP) - YEAR(BirthDate) - (RIGHT(CURRENT_TIMESTAMP, 5) < RIGHT(BirthDate, 5)) < 7
			) T
			GROUP BY F_ID
		) AS T2
		ON T1.F_ID = T2.F_ID
	';

	$result = $dbh->query($sql);

	close_db($dbh);

	return empty($result) ? array() : $result->fetchAll(PDO::FETCH_ASSOC);
}

function task1()
{
	$dbh = open_db();

	$sql = '
		SELECT F_ID, FullName, YEAR(CURRENT_TIMESTAMP) - YEAR(BirthDate) - (RIGHT(CURRENT_TIMESTAMP, 5) < RIGHT(BirthDate, 5)) AS Age
		FROM Faces
		WHERE YEAR(CURRENT_TIMESTAMP) - YEAR(BirthDate) - (RIGHT(CURRENT_TIMESTAMP, 5) < RIGHT(BirthDate, 5)) > 20
	';
	$result = $dbh->query($sql);

	close_db($dbh);

	return empty($result) ? array() : $result->fetchAll(PDO::FETCH_ASSOC);
}

function add_child($params)
{
	$dbh = open_db();

	$sql = '
		INSERT INTO Kids (F_ID, FullName, Name, Soname, FathersName, BirthDate)
		VALUES (:F_ID, :FullName, :Name, :Soname, :FathersName, :BirthDate)
	';
	$stmt = $dbh->prepare($sql);

	foreach ($params as $key => &$value) {
		$stmt->bindParam($key, $value);
	}

	$result = $stmt->execute();

	close_db($dbh);

	return $result;
}

function add_parent($params)
{
	$dbh = open_db();

	$sql = '
		INSERT INTO Faces (FullName, Name, Soname, FathersName, BirthDate)
		VALUES (:FullName, :Name, :Soname, :FathersName, :BirthDate)
	';
	$stmt = $dbh->prepare($sql);

	foreach ($params as $key => &$value) {
		$stmt->bindParam($key, $value);
	}

	$result = $stmt->execute();

	close_db($dbh);

	return $result;
}

function open_db()
{
	$dbh = new PDO('mysql:host=localhost;dbname=task6', 'root', '');
	return $dbh;
}

function close_db($dbh)
{
	$dbh = null;
}

function get_value(&$var, $default = null)
{
	return isset($var) ? $var : $default;
}
