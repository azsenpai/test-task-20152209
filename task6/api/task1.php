<?php

require_once 'model.php';

$result = task1();

print json_encode(array(
	'status' => 'ok',
	'html' => include('views/task1_view.php'),
));
