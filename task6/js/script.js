jQuery(function($) {
	var $parent_name = $('[name="parent_name"]');
	var $parent_id = $('[name="parent_id"]');

	var cache_parent_name = {};

	$parent_name.on('focus', function() {
		if ($parent_name.val().length == 0) {
			$parent_name.autocomplete('search', '');
		}
	});

	$parent_name.autocomplete({
		minLength: 0,
		source: parent_name_source,
		focus: function(event, ui) {
			$parent_name.val(ui.item.name);
			return false;
		},
		change: function(event, ui) {
			if (ui.item == null) {
				$parent_name.val('');
				$parent_id.val(0);
			}
		},
		select: function(event, ui) {
			$parent_name.val(ui.item.name);
			$parent_id.val(ui.item.id);

			return false;
		},
		messages: {
			noResults: '',
			results: function() {}
		}
	});

	$parent_name.autocomplete('instance')._renderItem = function(ul, item) {
		return $('<li>')
			.append(item.name)
			.appendTo(ul);
	};

	function parent_name_source(request, response) {
		var term = $.trim(request.term.toLowerCase());
		var option = '_' + term;

		if (option in cache_parent_name) {
			response(cache_parent_name[option]);
			return;
		}

		$.ajax({
			url: 'api/get_parents.php',
			type: 'post',
			data: {
				keyword: term
			},
			dataType: 'json',
			success: function(data) {
				if ('status' in data) {
					if (data.status == 'ok') {
						cache_parent_name[option] = data.parents;
						response(data.parents);
					}
				}
			}
		});
	}

	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		var $this = $(this);
		var $content = $($this.attr('href')).find('.content');

		cache_parent_name = {};

		if ($content.length == 0) {
			return;
		}

		$content.html('Пожалуйста подождите ....');

		$.ajax({
			url: 'api/' + $this.data('task') + '.php',
			type: 'post',
			dataType: 'json',
			success: function(data) {
				if ('status' in data) {
					if (data.status == 'ok') {
						$content.html(data.html);
					}
				}
			},
			error: function() {
			},
			complete: function() {
			}
		});
	});

	$('form.add-parent, form.add-child').find('[type="submit"]').on('click', function() {
		var $this = $(this);

		if ($this.hasClass('disabled')) {
			return;
		}

		var $form = $this.parents('form');

		var $status = $form.find('.status');
		var $pid = $form.find('[name="parent_id"]');

		var user = 'Ребенок';
		var pid = 0;

		if ($pid.length == 0) {
			user = 'Сотрудник';
		} else {
			pid = parseInt($pid.val());

			if (isNaN(pid) || pid == 0) {
				$status.html('Выберите родителя');
				return;
			}
		}

		var sname = $.trim($form.find('[name="sname"]').val());
		if (sname.length == 0) {
			$status.html('Введите фамилию');
			return;
		}

		var name = $.trim($form.find('[name="name"]').val());
		if (name.length == 0) {
			$status.html('Введите имя');
			return;
		}

		var fname = $.trim($form.find('[name="fname"]').val());
		if (fname.length == 0) {
			$status.html('Введите отчество');
			return;
		}

		var dob = $.trim($form.find('[name="dob"]').val());
		if (dob.length == 0) {
			$status.html('Выберите дату рождения');
			return;
		}

		$this.addClass('disabled');

		$status.html('Пожалуйста подождите ....');

		$.ajax({
			url: $form.attr('action'),
			type: $form.attr('method'),
			data: {
				pid: pid,
				sname: sname,
				name: name,
				fname: fname,
				dob: dob
			},
			dataType: 'json',
			success: function(data) {
				if ('status' in data) {
					if (data.status == 'ok') {
						$status.html(user + ' успешно добавлен');
					}
				}
			},
			error: function() {
			},
			complete: function() {
				$this.removeClass('disabled');
			}
		});
	});

	$('form').on('submit', function(e) {
		e.preventDefault();
	});

	$('.btn.clear').on('click', function() {
		var $form = $(this).parents('form');

		$form.find('input').not('[type="submit"]').val('');
		$form.find('.status').html('');
	});

	$('[name="dob"]').datepicker({
		format: "yyyy-mm-dd",
		language: "ru",
		autoclose: true,
	});
});
