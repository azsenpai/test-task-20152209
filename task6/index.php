<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta content="width=device-width, initial-scale=1.0" name="viewport">
		<title>task6</title>
		<link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="lib/bootstrap-datepicker/bootstrap-datepicker.min.css">
		<link rel="stylesheet" href="css/style.css">
		<!--[if lt IE 9]>
			<script type="text/javascript" src="js/html5shiv.min.js"></script>
		<![endif]-->		
	</head>
	<body>
		<div class="main-wrapper">
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#t1" role="tab" data-toggle="tab">Добавить физ. лицо</a></li>
				<li role="presentation"><a href="#t2" role="tab" data-toggle="tab">Добавить ребенка</a></li>
				<li role="presentation"><a href="#t3" role="tab" data-toggle="tab" data-task="task1">Задание 1</a></li>
				<li role="presentation"><a href="#t4" role="tab" data-toggle="tab" data-task="task2">Задание 2</a></li>
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="t1">
					<form class="add-parent" action="<?php print $_SERVER['REQUEST_URI'] . 'api/add_parent.php'; ?>" method="post">
						<div class="form-group input-group">
							<span class="input-group-addon">Фамилия</span>
							<input class="form-control" name="sname" type="text" placeholder="Введите фамилию">
						</div>
						<div class="form-group input-group">
							<span class="input-group-addon">Имя</span>
							<input class="form-control" name="name" type="text" placeholder="Введите имя">
						</div>
						<div class="form-group input-group">
							<span class="input-group-addon">Отчество</span>
							<input class="form-control" name="fname" type="text" placeholder="Введите отчество">
						</div>
						<div class="form-group input-group">
							<span class="input-group-addon">Дата рождения</span>
							<input class="form-control" name="dob" type="text" placeholder="Выберите дату" style="position: relative">
						</div>
						<div class="form-group text-center">
							<span class="btn btn-warning clear">Очистить поля</span>
							<input class="btn btn-primary submit" type="submit" value="Отправить">
						</div>
						<div class="status text-center"></div>
					</form>
				</div>
				<div role="tabpanel" class="tab-pane" id="t2">
					<form class="add-child" action="<?php print $_SERVER['REQUEST_URI'] . 'api/add_child.php'; ?>" method="post">
						<div class="form-group input-group">
							<span class="input-group-addon">Выберите родителя</span>
							<input class="form-control" name="parent_name" type="text" placeholder="Начните вводить имя">
							<input type="hidden" name="parent_id">
						</div>
						<div class="form-group input-group">
							<span class="input-group-addon">Фамилия</span>
							<input class="form-control" name="sname" type="text" placeholder="Введите фамилию">
						</div>
						<div class="form-group input-group">
							<span class="input-group-addon">Имя</span>
							<input class="form-control" name="name" type="text" placeholder="Введите имя">
						</div>
						<div class="form-group input-group">
							<span class="input-group-addon">Отчество</span>
							<input class="form-control" name="fname" type="text" placeholder="Введите отчество">
						</div>
						<div class="form-group input-group">
							<span class="input-group-addon">Дата рождения</span>
							<input class="form-control" name="dob" type="text" placeholder="Выберите дату" style="position: relative">
						</div>
						<div class="form-group text-center">
							<span class="btn btn-warning clear">Очистить поля</span>
							<input class="btn btn-primary submit" type="submit" value="Отправить">
						</div>
						<div class="status text-center"></div>
					</form>
				</div>
				<div role="tabpanel" class="tab-pane" id="t3">
					<p>Напишите SQL - запрос при помощи которого можно получить полное имя всех сотрудников старше 20 лет.</p>
					<div class="content"></div>
				</div>
				<div role="tabpanel" class="tab-pane" id="t4">
					<p>Напишите SQL - запрос при помощи которого можно получить список всех сотрудников и количество их детей младше 7 лет.</p>
					<div class="content"></div>
				</div>
			</div>
		</div>

		<script src="js/jquery-1.11.2.min.js"></script>
		<script src="js/jquery-ui.min.js"></script>

		<script src="lib/bootstrap/js/bootstrap.min.js"></script>

		<script src="lib/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
		<script src="lib/bootstrap-datepicker/locales/bootstrap-datepicker.ru.min.js"></script>

		<script src="js/script.js"></script>
	</body>
</html>
