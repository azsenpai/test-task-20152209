<?php

require_once 'model.php';

$result = task2();

print json_encode(array(
	'status' => 'ok',
	'html' => include('views/task2_view.php'),
));
