<?php

require_once 'model.php';

$keyword = get_value($_POST['keyword'], '');
$keyword = '%' . $keyword . '%';

print json_encode(array(
	'status' => 'ok',
	'parents' => get_parents($keyword),
));
