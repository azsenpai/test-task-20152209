<?php

require_once 'model.php';

$pid = (int)get_value($_POST['pid'], 0);
$sname = trim(get_value($_POST['sname'], ''));
$name = trim(get_value($_POST['name'], ''));
$fname = trim(get_value($_POST['fname'], ''));
$dob = trim(get_value($_POST['dob'], ''));

if (empty($pid) || empty($sname) || empty($name) || empty($fname) || empty($dob)) {
	print json_encode(array(
		'status' => 'error',
	));
	return;
}

$params = array(
	':F_ID' => $pid,
	':FullName' => $sname . ' ' . $name . ' ' . $fname,
	':Name' => $name,
	':Soname' => $sname,
	':FathersName' => $fname,
	':BirthDate' => $dob,
);

if (add_child($params)) {
	print json_encode(array(
		'status' => 'ok',
	));
} else {
	print json_encode(array(
		'status' => 'error',
	));
}
