<?php

$max_sum = 28;

$lower = 2;
$upper = 15;

while (true) {
	$sum = 0;

	for ($i = 0; $i < 4; $i ++) {
		$a[$i] = rand($lower, $upper);
		$sum += $a[$i];
	}

	print implode(' + ', $a) . ' = ' . $sum . PHP_EOL;

	if ($sum >= $max_sum) {
		break;
	}
}
